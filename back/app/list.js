const express = require("express");
const List = require("../models/List");
const Film = require("../models/Films");

const createRouter = () => {
    const router = express.Router();
    router.get("/", async (req, res) => {
        try {
            res.send(await List.find());
        } catch (e) {
            res.status(500).send(e)
        }
    });

    router.get("/:id", async (req, res) => {
        const list = await
            List.find({_id: req.params.id});
        res.send(list[0]);
    });

    router.post("/", async (req, res) => {
        const list = new List(req.body);
        try {
            await list.save();
            res.send(await List.find());
        } catch (e) {
            res.status(400).send(e)
        }
    });


    router.delete("/sessions", async (req, res) => {
        const list = await List.remove({_id: req.params.id});

        await list.save()

        res.send(await List.find());

    });

    return router;
};

module.exports = createRouter;