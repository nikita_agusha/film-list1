const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FilmSchema = new Schema({
    nameFilm: {
        type: String,
        required: true,
    },
    list: {
        type: Schema.Types.ObjectId,
        ref: "List",
        required: true
    },
});
const Film = mongoose.model("Film", FilmSchema);
module.exports = Film;