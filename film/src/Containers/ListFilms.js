import React, {useEffect, useState} from "react";
import axios from "axios";


const ListFilm = (props) => {
    const [films, setFilms] = useState([])
    const [nameFilm, setNameFilm] = useState("")
    const [nameList, setNameList] = useState("")
    console.log(props.match.params.id)

    useEffect(() => {
        axios.get("http://localhost:8000/list/" + props.match.params.id).then(r => setNameList(r.data.nameList))
        axios.get("http://localhost:8000/film/" + props.match.params.id).then(r => {
            setFilms(r.data)
        })
    }, [])

    const changeAddNewFilm = (e) => {
        setNameFilm(e.target.value.trim())
    }
    const addNewFilm = () => {
        axios.post("http://localhost:8000/film", {
            nameFilm: nameFilm,
            list: props.match.params.id
        }).then(r => {
            setNameFilm("")
            setFilms(r.data)
        })
    }
    const deleteFilm = (id, listId) => {
        axios.delete("http://localhost:8000/film/" + id,).then(r => setFilms(r.data))
    }


    return (<>
            <div style={{margin: "10px 0"}}>
                <input type="text" value={nameFilm} placeholder="enter name film" name="nameFilm" onChange={changeAddNewFilm}/>
                <button onClick={addNewFilm} style={{
                    marginLeft: "10px",
                    background: "cadetblue",
                    color: "white",
                    border: "none",
                    padding: "10px",
                    cursor:"pointer"
                }}>Add new film
                </button>
            </div>
            <p style={{fontSize: "21px", padding: "10px 0"}}> Name list: {nameList}</p>
            {(films.length > 0) ? films.map(list => {
                return (
                    <div key={list._id}
                         style={{
                             border: "1px solid",
                             padding: "10px",
                             margin: "10px 0",
                             fontSize: "21px",
                             display: "flex",
                             justifyContent: "space-between"
                         }}>
                        <p key={list._id} style={{    padding: "10px 0"}}>name: {list.nameFilm}</p>
                        <button onClick={() => deleteFilm(list._id, props.match.params.id)} style={{
                            marginLeft: "10px",
                            background: "cadetblue",
                            color: "white",
                            border: "none",
                            padding: "10px",
                            cursor:"pointer"
                        }}>delete
                        </button>
                    </div>

                )
            }) : null

            }
        </>
    )
}

export default ListFilm