import React, {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";


const Container = () => {
    const [lists, setLists] = useState([])
    const [nameList, setNameList] = useState("")

    useEffect(() => {
        axios.get("http://localhost:8000/list").then(r => setLists(r.data))
    }, [])

    const changeAddNewList = (e) => {
        setNameList(e.target.value.trim())
    }
    const addNewList = () => {
        axios.post("http://localhost:8000/list", {nameList: nameList}).then(r => setLists(r.data))
    }
    lists.map(list => {
        axios.get("http://localhost:8000/film/" + list._id).then(r => {
            return list.colFilm = r.data.length
        })

    })

    return (<div>
            <div style={{margin: "10px 0"}}>

                <input type="text" placeholder="enter name list" name="nameList" onChange={changeAddNewList}/>
                <button onClick={addNewList} style={{
                    marginLeft: "10px",
                    background: "cadetblue",
                    color: "white",
                    border: "none",
                    padding: "10px",
                    cursor:"pointer"

                }}>Add new list
                </button>
            </div>

            {
                lists.map(list => {
                    return (
                        <div key={list._id} style={{
                            border: "1px solid", display: "flex",
                            justifyContent: "space-between", padding: "10px", margin: "10px 0"
                        }}>
                            <div>
                                <p key={list._id}>name: {list.nameList}</p>
                                <p style={{padding:"10px 0"}}> ColFilm: {list.colFilm}</p>
                            </div>
                            <Link to={{pathname: 'list/' + list._id,}} style={{
                                marginLeft: "10px",
                                background: "cadetblue",
                                color: "white",
                                border: "none",
                                padding: "10px",
                                textDecoration:"none",

                            }}>Info list</Link>
                        </div>
                    )
                })
            }

        </div>
    )
}

export default Container