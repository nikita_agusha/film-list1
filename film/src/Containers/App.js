import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Container from "./Container";
import ListFilms from "./ListFilms";

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <div style={{
                    width: "1200px",
                    margin: "0 auto"
                }}>
                    <Route path="/" exact component={Container}/>
                    <Route path="/list/:id" exact component={ListFilms}/>
                </div>

            </Switch>


        </BrowserRouter>
    );
}

export default App;
